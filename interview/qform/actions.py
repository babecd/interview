__author__ = 'user'
from .models import QuestionModel, Answers


def get_answers(kwargs):
    questions = QuestionModel.objects.filter(form=kwargs['object'])
    all = {}
    for foo in questions:
        local = {foo: Answers.objects.filter(question=foo).values('answer')}
        all.update(local)

    return all