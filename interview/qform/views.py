__author__ = 'user'


from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView, DetailView, ListView, FormView, UpdateView, CreateView
from .models import QuestionModel, UserAnswer, Answers, TypeAnswer, PageModel
import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.views import login, logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from .actions import get_answers

class SuccessView(TemplateView):
    template_name = 'qform/success.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {"request": request})


class FirstView(TemplateView):
    template_name = 'qform/first.html'


class Registration(FormView):
    form_class = UserCreationForm
    success_url = reverse_lazy('main')
    template_name = 'qform/registration.html'

    def form_valid(self, form):
        self.object = form.save()
        username = self.request.POST['username']
        password = self.request.POST['password1']
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super(Registration, self).form_valid(form)


class Login(FormView):
    form_class = AuthenticationForm
    success_url = reverse_lazy('main')
    template_name = 'qform/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(FormView):
    template_name = 'qform/logout.html'

    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse_lazy('login'))


# class AjaxView(TemplatelView):
#     template_name = 'qform/question.html'
#
#     @method_decorator(login_required(login_url=reverse_lazy('first')))
#     def dispatch(self, *args, **kwargs):
#         return super(AjaxView, self).dispatch(*args, **kwargs)


class MainView(FormView):
    form_class = forms.MainForm
    template_name = 'qform/questionmodel_form.html'
    success_url = reverse_lazy('success')
    queryset = QuestionModel.objects.all()

    @method_decorator(csrf_exempt)
    @method_decorator(login_required(login_url=reverse_lazy('first')))
    def dispatch(self, *args, **kwargs):
        return super(MainView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form = self.get_form()
        if form.is_valid():
            print self.request
            question = form.save(commit=False)
            question.owner = self.request.user
            question.save()
            for answer in self.request.POST.getlist('answer'):
                Answers.objects.create(question=question, answer=answer)
            answ = Answers.objects.filter(question=question)
        if self.request.is_ajax():
            return render_to_response('qform/question.html', {"object": question, "answers": answ})
        return super(MainView, self).form_valid(form)


class ListFormView(ListView):
    template_name = 'qform/list_forms.html'
    model = PageModel

    def get(self, request, *args, **kwargs):
        data = PageModel.objects.filter(owner=request.user)
        return render_to_response(self.template_name, {"data": data, 'request': request})


# class ListView(ListView):
#     model = QuestionModel
#     template_name = "qform/list.html"
#
#     def get(self, request, *args, **kwargs):
#         form = PageModel.objects.filter(owner=request.user)
#         data = QuestionModel.objects.filter(form=form)
#         return render_to_response(self.template_name, {"data": data, 'request': request, 'form': form})


class ListQuestionView(DetailView):
    template_name = 'qform/list_question.html'
    model = PageModel

    def get_context_data(self, **kwargs):
        context = super(ListQuestionView, self).get_context_data(**kwargs)
        answers = get_answers(kwargs)
        context["data"] = QuestionModel.objects.filter(form=kwargs['object'])
        context["answers"] = get_answers(kwargs)
        context["request"] = self.request
        return context


class NewFormView(CreateView):
    model = PageModel
    fields = ['form']


class VoteView(DetailView, CreateView):
    template_name = 'qform/vote.html'
    model = PageModel
    form_class = forms.UserAnswersForm

    def get_context_data(self, **kwargs):
        print kwargs
        context = super(VoteView, self).get_context_data(**kwargs)
        questions = QuestionModel.objects.filter(form=self.object)
        print questions
        context['questions'] = questions
        context['answers'] = get_answers(kwargs)
        return context

    def post(self, request, *args, **kwargs):
        qu = QuestionModel.objects.filter(id__in=request.POST.getlist('question'))
        for q in qu:
            if request.POST.getlist('answer%d' % q.id) == []:
                return HttpResponseRedirect(reverse_lazy('error'))
            answers = Answers.objects.get(answer=request.POST.get('answer%d' % q.id))
            answer = UserAnswer(user=request.user, question=q, answer=answers)
            answer.save()
        return HttpResponseRedirect(reverse_lazy('success'))



class ListVoteView(ListView):
    template_name = 'qform/list_vote.html'
    model = PageModel

    def get_context_data(self, **kwargs):
        context = super(ListVoteView, self).get_context_data(**kwargs)
        context["object"] = PageModel.objects.filter(owner=self.request.user)
        return context


class ChangeQuestionView(UpdateView):
    template_name = 'qform/update_question.html'
    model = QuestionModel
    success_url = reverse_lazy('success')
    fields = ['question', 'type_answer', 'form']

    def get_context_data(self, **kwargs):
        context = super(ChangeQuestionView, self).get_context_data(**kwargs)
        context['answers'] = Answers.objects.filter(question=self.object)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.save()
        question = self.object
        answers = Answers.objects.filter(question=question)
        print request.POST
        for answer in answers:
            answer.answer = request.POST['answer%d' % answer.pk]
            answer.save()
        return super(ChangeQuestionView, self).post(request, *args, **kwargs)


class ResultsView(ListView):
    template_name = 'qform/results.html'
    model = UserAnswer

    def get(self, request, *args, **kwargs):
        result = UserAnswer.objects.filter(question__id=kwargs['pk'])
        return render_to_response(self.template_name, {"results": result, "request": request})


class AllResultsView(ListView):
    template_name = 'qform/all_results.html'
    model = UserAnswer

    def get(self, request, *args, **kwargs):
        result = UserAnswer.objects.filter(user=request.user)
        return render_to_response(self.template_name, {"results": result, "request": request})


class AnswersView(ListView):
    template_name = 'qform/all_results.html'
    model = UserAnswer

    def get(self, request, *args, **kwargs):
        form = PageModel.objects.filter(owner=request.user)
        question = QuestionModel.objects.filter(form=form)
        question_owner = UserAnswer.objects.filter(question=question)
        return render_to_response(self.template_name, {"results": question_owner, "request": request})


class ErrorView(TemplateView):
    template_name = 'qform/error.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {"request": request})