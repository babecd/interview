from django import forms
from .models import QuestionModel, UserAnswer, Answers


class MainForm(forms.ModelForm):
    class Meta:
        model = QuestionModel
        fields = ['question', 'type_answer', 'form']
        readonly_fields = ['created_date']
        widgets = {
            'owner': forms.HiddenInput(),
        }


class UserAnswersForm(forms.ModelForm):
    class Meta:
        model = UserAnswer
        fields = ['question', 'user', 'answer']


