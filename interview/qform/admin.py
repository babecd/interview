from django.contrib import admin

from .models import QuestionModel, Answers, UserAnswer, TypeAnswer, PageModel


class PageQusetion(admin.StackedInline):
    model = QuestionModel


class Choices(admin.StackedInline):
    model = Answers


class PageAdmin(admin.ModelAdmin):
    fields = ['form', 'owner']
    list_display = ['form', 'owner']
    inlines = [PageQusetion]


class QuestionAdmin(admin.ModelAdmin):
    fields = ['question', 'type_answer', 'form', 'created_date']
    list_display = ['question', 'type_answer', 'created_date']
    inlines = [Choices]


class AnswerAdmin(admin.ModelAdmin):
    fields = ['question', 'answer']
    list_display = ['question', 'answer']


class UserAnswerAdmin(admin.ModelAdmin):
    fields = ['question', 'user', 'answer']
    list_display = ['question', 'user', 'answer']


class TypeAnswerAdmin(admin.ModelAdmin):
    fields = ['type_answer']


admin.site.register(QuestionModel, QuestionAdmin)
admin.site.register(Answers, AnswerAdmin)
admin.site.register(UserAnswer, UserAnswerAdmin)
admin.site.register(TypeAnswer, TypeAnswerAdmin)
admin.site.register(PageModel, PageAdmin)