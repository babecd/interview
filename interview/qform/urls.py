__author__ = 'user'

from django.conf.urls import include, url
import views
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^list_form/$', views.ListFormView.as_view(), name="list_form"),
    url(r'^question/$', login_required(views.MainView.as_view()), name="main"),
    url(r'^first/$', views.FirstView.as_view(), name="first"),
    url(r'^new_form/$', views.NewFormView.as_view(), name="form"),

    url(r'^list_form/(?P<pk>\d+)/$', views.ListQuestionView.as_view(), name="in_form"),

    # url(r'^list_question/$', views.ListView.as_view(), name="list"),
    url(r'^vote/(?P<pk>\d+)/$', views.VoteView.as_view(), name="vote"),
    url(r'^list_vote/$', views.ListVoteView.as_view(), name="list_vote"),

    url(r'^success/$', views.SuccessView.as_view(), name="success"),
    url(r'^error/$', views.ErrorView.as_view(), name="error"),

    url(r'^results/(?P<pk>\d+)/$', views.ResultsView.as_view(), name="results"),
    url(r'^all_results/$', views.AllResultsView.as_view(), name="all_results"),
    url(r'^answers/$', views.AnswersView.as_view(), name="answers"),

    url(r'^accounts/login/$', views.Login.as_view(), name="login"),
    url(r'^accounts/logout/$', views.LogOut.as_view(), name="logout"),
    url(r'^accounts/registration/$', views.Registration.as_view(), name="registr"),


    # url(r'^question/$', views.AjaxView.as_view(), name="ajax"),
    url(r'^question_update/(?P<pk>\d+)/$', views.ChangeQuestionView.as_view(), name="update"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)