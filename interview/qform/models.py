from django.db import models
from django.contrib.auth.models import User
import datetime


class TypeAnswer(models.Model):
    type_answer = models.CharField(max_length=30)

    def __unicode__(self):
        return self.type_answer


class PageModel(models.Model):
    form = models.CharField(max_length=200, default="New form")
    owner = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.form


class QuestionModel(models.Model):
    question = models.CharField(max_length=180)
    description = models.CharField(max_length=300, blank=True)
    type_answer = models.ForeignKey(TypeAnswer)
    form = models.ForeignKey(PageModel)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return '{0} {1} '.format(self.question,
                                 self.description)


class Answers(models.Model):
    question = models.ForeignKey(QuestionModel)
    answer = models.CharField(max_length=180)

    def __unicode__(self):
        return self.answer


class UserAnswer(models.Model):
    question = models.ForeignKey(QuestionModel)
    user = models.ForeignKey(User)
    answer = models.ForeignKey(Answers)

    # class Meta:
    #     unique_together = (('question', 'user'),)

    def __unicode__(self):
        return u'{0} {1} {2}'.format(self.question, self.answer, self.user)
