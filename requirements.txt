Django==1.8.4
argparse==1.2.1
django-registration==1.0
wsgiref==0.1.2
psycopg2==2.6.1
